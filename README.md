# MutSaSNS(멋사스네스)
### 멋쟁이사자처럼 백엔드스쿨 2기의 학습 내용 정리를 위한 프로젝트
GOAL | 로그인, 글쓰기, 수정, 삭제, 페이징 등 게시판 기본 기능 구현
<br>
[API.DOCS 바로가기](http://ec2-3-34-204-179.ap-northeast-2.compute.amazonaws.com:8080/v2/api-docs)

## 개발 환경
- 에디터 : Intellij Ultimate
- 개발 툴 : SpringBoot 2.7.5
- 자바 : JAVA 11
- 빌드 : Gradle 6.8
- 서버 : AWS EC2
- 배포 : Docker
- 데이터베이스 : MySql 8.0
- 필수 라이브러리 : SpringBoot Web, MySQL, Spring Data JPA, Lombok, Spring Security

## 프로젝트 요약
### 1. ERD 다이어그램
![mission01ERD](https://user-images.githubusercontent.com/61702307/211475688-1e071e8b-b396-4d4f-8493-875e66da4992.png)

### 2. 클래스 다이어그램
- 업로드 예정

### 3. 아키텍처 구조
- 레이어드 아키텍처 적용
![Layered Architecture](https://user-images.githubusercontent.com/61702307/211436308-97a88695-7a46-420f-8902-23f6e57e0669.png)

### 4. 체크리스트
- [x] 인증 / 인가 필터 구현
- [x] 상품 조회 / 수정 / 삭제 API 구현
- [x] 상품 수정 테스트 코드 작성
- [x] Swagger를 이용해 상품 수정 API 자동화
- [x] develop 브랜치에 push할 경우 애플리케이션 AWS EC2 서버에 자동으로 배포되도록 구현

### 5. 엔드포인트 일람

| 기능          | HTTP메서드 | URL                  |
|:------------|--------|----------------------|
| 회원가입        |    POST | `api/v1/users/join`  |
| 로그인         |    POST | `api/v1/users/login` |
| 포스트 등록      |    POST | `/api/v1/posts`      |
| 포스트 조회(전체)  |     GET | `/api/v1/posts`      |
| 포스트 조회(상세)  |     GET | `/posts/{postId}`    |
| 포스트 수정      |     PUT | `/posts/{postId}`    |
| 포스트 삭제      |  DELETE | `/posts/{postId}`    |

## 요구사항 분석
### 1. 회원가입, 로그인 `/api/v1/users`
   - [POST] 회원가입 `/join`
     - 회원가입 성공 시 "회원가입 성공" 리턴
   - [POST] 로그인 `/login`
     - Spring Security, JWT 사용해 구현
     - 로그인 성공 시 token 리턴
### 2. 게시글 CRUD `/api/v1/posts`
   - [POST] 포스트 등록 `/posts`
     - 회원만 글 작성 가능
   - [GET] 포스트 리스트 조회 (전체 조회) `/posts`
     - 회원, 비회원 모두 조회 가능
     - 제목, 글쓴이, 마지막 수정일자 표시
     - 페이징 기능 (Pageable) 포함
       - 한 페이지 당 default 피드 갯수는 20개
       - 총 페이지 갯수 표시
       - 작성날짜 기준으로 최신순으로 Sort
   - [GET] 포스트 상세 (1개 조회) `/posts/{postId}`
     - 회원, 비회원 모두 조회 가능
     - 글의 제목, 내용, 글쓴이, 작성 날짜, 마지막 수정일자 표시
   - [PUT] 포스트 수정 `/posts/{postId}`
     - 글을 작성한 일반 회원, ADMIN 회원이 글에 대한 수정과 삭제 가능
   - [DELETE] 포스트 삭제 `/posts/{postId}`
     - 글을 작성한 일반 회원, ADMIN 회원이 글에 대한 수정과 삭제 가능

### 3. 에러처리
- 개요
  - ErrorCode에서 정의한 HttpStatus를 StatusCode로 Return
  - result.errorCode가 존재함 (ex: INVALID_USER_NAME)
  - messgae가 존재함
    - 예시
      ```json
      {
          "resultCode": "ERROR",
          "result": {
              "errorCode": "POST_NOT_FOUND",
              "message": "Post not founded"
          }
      }
      ```
- 에러 코드

    |  에러 코드           |설명| 상태 코드             |
    |--------------------|:------------------|:-------------|
    | INVALID_USER_NAME |중복되는 USERNAME| CONFLICT(409)     |
    | INVALID_PASSWORD |일치하지 않는 PASSWORD| UNAUTHORIZED(401) |
    | INVALID_TOKEN    |잘못된 토큰| UNAUTHORIZED(401) |
    | INVALID_PERMISSION |사용자 권한 없음| UNAUTHORIZED(401) |
    | POST_NOT_FOUND   |해당 포스트 없음| NOT_FOUND(404)    |
    | USERNAME_NOT_FOUND |USERNAME을 찾을 수 없음| NOT_FOUND(404)    |
    | DATABASE_ERROR   |INTERNAL_SERVER_ERROR| DB ERROR(500)     |

### 4. 테스트
- 업로드 예정

## 엔드 포인트
- 기본 URL : `/api/v1`
- JSON 형식

### 1. 회원 인증/인가
- 회원 URL : `/users`
- [POST] 회원가입 `/join`
  - 입력
    ```json
    {
        "username" : "user1",
        "password" : "123456"
    }
    ```
  - 리턴
    ```json
    {
         "resultCode": "SUCCESS",
         "result": {
            "userId": 1,
            "userName": "user1"
        }
    }
    ```
- [POST] 로그인 `/login`
  - 입력
      ```json
    {
         "username" : "user1",
         "password" : "123456"
    }
    ```
  - 리턴
    ```json
    {
        "resultCode": "SUCCESS",
        "result": {
            "userId": 1,
            "userName": "user1"
        }
    }
    ```

### 2. 포스트
- 포스트 URL : `/posts`
- [POST] 포스트 등록
    - 입력
      ```json
      {
          "title" : "title1",
          "body" : "body1"
      }
      ```
    - 리턴
      ```json
      {
            "resultCode": "SUCCESS",
            "result": {
               "message": "포스트 등록 완료",
               "postId": 0
           }
      }
      ```
- [GET] 포스트 리스트 조회
    - id, 제목, 내용, 작성날짜, 수정날짜
    - 최신 순으로 20개씩 표시 (Pageable)
    - 등록일자 기준 최신순 정렬
    - 리턴
      ```json
      {
          "resultCode": "SUCCESS",
          "result": {
              "content": [
                  {
                      "id": 1,
                      "title": "title1",
                      "body": "body1",
                      "userName": "user1",
                      "createdAt": "2022/01/09 10:10:10",
                      "createdAt": "2022/01/09 11:11:11"
                  },
                  {
                      "id": 2,
                      "title": "title1",
                      "body": "body1",
                      "userName": "user1",
                      "createdAt": "2022/01/09 10:10:10",
                      "createdAt": "2022/01/09 11:11:11"
                  },      
                  {
                      "id": 3,
                      "title": "title1",
                      "body": "body1",
                      "userName": "user1",
                      "createdAt": "2022/01/09 10:10:10",
                      "createdAt": "2022/01/09 11:11:11"
                  }
              ],
              "pageable": "INSTANCE",
              "last":true,
              "totalPages": 1,
              "totalElements": 4,
              "size": 4,
              "number": 0,
              "sort": {
                  "empty": true,
                  "sorted": false,
                  "unsorted": true
              },
              "first": true,
              "numberOfElements": 4,
              "empty": false
          }
      }
      ```
- [GET] 포스트 상세 조회
    - id, 제목, 내용, 작성자, 작성날짜, 수정날짜
    - 리턴
      ```json
      {
          "resultCode": "SUCCESS",
          "result": {
              "id": 1,
              "title": "title1",
              "body": "body1",
              "createdAt": yyyy-mm-dd hh:mm:ss,
              "lastModifiedAt": yyyy-mm-dd hh:mm:ss
          }
      }
      ```
- [PUT] 포스트 수정
    - 입력
      ```json
      {
          "title" : "modified title1",
          "body" : "modified body1"
      }
      ```
    - 리턴
      ```json
      {
          "resultCode": "SUCCESS",
          "result": {
              "message": "포스트 삭제 완료",
              "postId": 0
          }
      }
      ```
- [DELETE] 포스트 삭제
    - 리턴
      ```json
      {
          "resultCode": "SUCCESS",
          "result": {
              "message": "포스트 삭제 완료",
              "postId": 0
          }
      }
      ```

<br>

***
하단 업로드 예정
- 접근 방법
- 특이사항
- 구현 과정에서 아쉬웠던 점
- 궁금했던 점
