package com.example.final01.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

//Annotation 없이
class AlgorithmServiceTest {

    AlgorithmService algorithmService = new AlgorithmService();

    @Test
    @DisplayName("자릿수의 합 잘 구하는지")
    void sumOfDigit() {
        assertEquals("21", algorithmService.sumOfDigit(687));
    }
}