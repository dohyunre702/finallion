package com.example.final01.service;

import com.example.final01.domain.dto.post.PostModifyRequest;
import com.example.final01.domain.Post;
import com.example.final01.domain.User;
import com.example.final01.domain.dto.post.PostCreateRequest;
import com.example.final01.domain.dto.post.PostDto;
import com.example.final01.domain.dto.post.PostResponse;
import com.example.final01.enums.ErrorCode;
import com.example.final01.exception.SignException;
import com.example.final01.repository.PostRepository;
import com.example.final01.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    //등록
    public PostDto create(PostCreateRequest request, String userName) {
        //실패 1. 유저가 존재하지 않음
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new SignException(ErrorCode.INVALID_PERMISSION, "회원만 글을 작성할 수 있습니다."));

        Post savedPost = postRepository.save(Post.toEntity(request.getTitle(), request.getBody(), user));
        return PostDto.builder()
                .id((int) savedPost.getId())
                .build();
    }

    //전체 조회
    public Page<PostDto> getAll(Pageable pageable) {
        Page<Post> posts = postRepository.findAll(pageable);
        Page<PostDto> postList = PostDto.getPostList(posts);
        return postList;
    }

    //상세 조회
    public PostDto getById(Integer id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new SignException(ErrorCode.POST_NOT_FOUND, "존재하지 않는 포스트입니다"));

        PostDto postdto = PostDto.getPost(post);
        return postdto;
    }

    //수정
    public PostResponse modifyPost(Integer id, PostModifyRequest postRequest, String userName) {
        //실패 1. 포스트 존재하지 않음
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new SignException(ErrorCode.POST_NOT_FOUND, "원하는 게시글을 찾을 수 없습니다"));

        //실패 2. 유저 존재하지 않음
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new SignException(ErrorCode.USERNAME_NOT_FOUND, "해당 유저가 없습니다."));

        //실패 3. 작성자 불일치
        if (post.getUser().getUserName() != user.getUserName()) {
            throw new SignException(ErrorCode.INVALID_PERMISSION, "글을 작성한 유저만 수정할 수 있습니다.");
        }

        //수정
        Post.updatePost(postRequest.getTitle(), postRequest.getBody());

        //값 반환
        return PostResponse.builder()
                .postId((int) post.getId())
                .build();
    }

    //삭제
    public PostResponse deletePost(Integer id, String userName) {
        //실패 1. 포스트 존재하지 않음
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new SignException(ErrorCode.POST_NOT_FOUND, "원하는 게시글을 찾을 수 없습니다"));

        //실패 2. 유저 존재하지 않음
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new SignException(ErrorCode.USERNAME_NOT_FOUND, "해당 유저가 없습니다."));

        //삭제
        postRepository.deleteById(id);

        return PostResponse.builder()
                .postId((int) post.getId())
                .build();
    }
}
