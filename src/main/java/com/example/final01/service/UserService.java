package com.example.final01.service;

import com.example.final01.domain.User;
import com.example.final01.domain.dto.user.UserDto;
import com.example.final01.domain.dto.user.UserJoinRequest;
import com.example.final01.enums.ErrorCode;
import com.example.final01.exception.SignException;
import com.example.final01.repository.UserRepository;
import com.example.final01.util.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;


    //로그인 키
    @Value("${jwt.token.secret}")
    private String secretKey;
    private long expireTimeMs = 1000 * 60 * 60; //1시간

    //회원가입
    public UserDto join(UserJoinRequest userJoinRequest) {
        userRepository.findByUserName(userJoinRequest.getUserName())
                //중복 체크
                .ifPresent(user -> {
                    throw new SignException(ErrorCode.DUPLICATED_USER_NAME, "");
                });

        //없으면 저장
        User savedUser = userRepository.save(userJoinRequest.toEntity(encoder.encode(userJoinRequest.getPassword())));

        //builder로 반환
        return UserDto.builder()
                .id(savedUser.getUserId())
                .userName(savedUser.getUserName())
                .password(savedUser.getPassword())
                .build();
    }

    //로그인
    public String login(String userName, String password) {
        //id가 존재하지 않을 때 에러 반환, 존재할 때 user에 담기
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new SignException(ErrorCode.USERNAME_NOT_FOUND, String.format("%s는 존재하지 않습니다.", userName)));

        //encoder로 user의 password 매치. 일치하지 않으면 invalid password
        if(!encoder.matches(password, user.getPassword())) {
            throw new SignException(ErrorCode.INVALID_PASSWORD, String.format("username 또는 password가 잘못되었습니다."));
        }
        //토큰값 반환
        return JwtTokenUtil.createToken(userName, secretKey, expireTimeMs);
    }
}
