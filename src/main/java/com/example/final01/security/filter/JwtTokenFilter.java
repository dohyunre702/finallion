package com.example.final01.security.filter;

import com.example.final01.service.UserService;
import com.example.final01.util.JwtTokenUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {
    private final UserService userService;
    private final String secretKey;
    @Override
    protected  void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);

        //1. jwt를 Token으로 보내지 않은 경우
        if (authorization == null || !authorization.startsWith("$2a$10")) {
            filterChain.doFilter(request, response);
            return;
        }

        //2. Bearer로 시작하지 않아 token 추출에 실패한 경우
        String token;
        try {
            token = authorization.split(" ")[1]; //순수 토큰만 추출
        } catch (Exception e) {
            filterChain.doFilter(request, response);
            return;
        }

        //3. 2)에서 실패 - jwt가 유효하지 않은 경우
        if (isExpired(token, secretKey)) {
            filterChain.doFilter(request, response);
            return;
        }

        //정상적인 토큰을 가졌을 때 문 열어주기
        //token에서 username 꺼내기
        String userName = JwtTokenUtil.getUserName(extractClaims(token, secretKey));
        //권한 부여
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                userName, null, List.of(new SimpleGrantedAuthority("USER")));
        //Detail 넣기
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        filterChain.doFilter(request, response);
    }


    //key로 token을 열어서 토큰 만료 일자를 추출한다. Claims 타입으로 반환
    public static Claims extractClaims(String token, String secretKey) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }

    //꺼낸 내용 중, 만료기한에 관한 정보를 사용해 현재 시각과 비교해 만료 판별
    public static boolean isExpired(String token, String secretKey) {
        Date expiredDate = extractClaims(token, secretKey).getExpiration();
        return expiredDate.before(new Date());
        //1) 만료일자가 현재 시간 전 : 유효기간이 끝남 = true
        //2) 만료일자가 현재 시간 이후 : 만료 전 = false
    }

}
