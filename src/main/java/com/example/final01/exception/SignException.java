package com.example.final01.exception;

import com.example.final01.enums.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SignException extends RuntimeException {
    private ErrorCode errorCode;
    private String message;

    public String toString() {
        if(message == null) return errorCode.getMessage();
        return String.format("%s. %s", errorCode.getMessage(), message);
    }

}
