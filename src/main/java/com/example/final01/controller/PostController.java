package com.example.final01.controller;

import com.example.final01.domain.dto.Response;
import com.example.final01.domain.dto.post.*;
import com.example.final01.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
public class PostController {
    private final PostService postService;

    //등록
    @PostMapping("")
    public Response<PostResponse> create(@RequestBody PostCreateRequest postCreateRequest, Authentication authentication) {
        String userName = authentication.getName();
        PostDto postDto = postService.create(postCreateRequest, userName);
        return Response.success(new PostResponse("포스트 등록 완료", postDto.getId()));
    }

    //전체 조회
    @GetMapping("")
    public Response<Page<PostDto>> getAll(@PageableDefault(size = 20) @SortDefault(sort= "createdAt", direction = Sort.Direction.DESC) Pageable pageable) {
        Page<PostDto> postList = postService.getAll(pageable);
        return Response.success(postList);
    }

    //상세 조회
    @GetMapping("/{id}")
    public Response<PostDto> getById(@PathVariable Integer id) {
        PostDto postDto = postService.getById(id);
        return Response.success(postDto);
    }

    //수정
    @PutMapping("/{id}")
    public Response<PostResponse> modify(@PathVariable Integer id, @RequestBody PostModifyRequest postRequest, Authentication authentication) {
        PostResponse postResponse = postService.modifyPost(id, postRequest, authentication.getName());
        return Response.success(new PostResponse("포스트 수정 완료", postResponse.getPostId()));
    }

    //삭제
    @DeleteMapping("/{id}")
    public Response<PostResponse> delete(@PathVariable Integer id, Authentication authentication) {
        PostResponse postDto = postService.deletePost(id, authentication.getName());
        return Response.success(new PostResponse("포스트 삭제 완료", postDto.getPostId()));
    }

}
