package com.example.final01.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Response<T> {
    private String resultCode;
    private T result;

    public static <T> Response<T> error(T result){
        return new Response<>("ERROR", result);
    }
    //    public static Response<Void> error(String resultCode){
    //        return new Response<>(resultCode, null);
    //    }
    public static <T> Response<T> success(T result){
        return new Response<>("SUCCESS", result);
    }
}
