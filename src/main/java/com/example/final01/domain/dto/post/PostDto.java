package com.example.final01.domain.dto.post;

import com.example.final01.domain.BaseEntity;
import com.example.final01.domain.Post;
import com.example.final01.domain.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostDto extends BaseEntity {
    private int id;
    private String userName;
    private String title;
    private String body;

    public static Page<PostDto> getPostList(Page<Post> posts) {
        return posts.map(postMap -> PostDto.builder()
                .id((int) postMap.getId())
                .title(postMap.getTitle())
                .body(postMap.getBody())
                .userName(postMap.getUser().getUserName())
                .build());
    }

    public static PostDto getPost(Post post) {
        return PostDto.builder()
                .id((int) post.getId())
                .title(post.getTitle())
                .body(post.getBody())
                .userName(post.getUser().getUserName())
                .build();
    }
}
