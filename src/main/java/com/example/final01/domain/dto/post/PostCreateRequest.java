package com.example.final01.domain.dto.post;

import com.example.final01.domain.Post;
import com.example.final01.domain.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class PostCreateRequest {
    private final String title;
    private final String body;

}
