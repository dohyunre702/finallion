package com.example.final01.domain.dto.user;

import com.example.final01.domain.BaseEntity;
import com.example.final01.domain.User;
import lombok.*;
import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto extends BaseEntity {
    private int id;
    private String userName;
    private String password;
    private int role; //1: normal, 2: admin

}
